README
======

Machine de Turing
-----------------
- **Auteur**:	NUEL Guillaume
- **Date**:	30/11/2012
- **Repo**:	https://bitbucket.org/ImmortalPC/calcucomplex


Description
-----------
Ce programme permet d'executer des **Machines de Turing** avec un language binaire ou non.


Compilation
-----------
Pour compiler ce programme, il est nécéssaire d'avoir les outils suivants:

- GCC 4.X / GNU
- make / GNU


Format du code
--------------
- Le code est en C99.
- Le code est en UTF-8
- Le code est indenté avec des tabulations réel (\t)
- Les fins de lignes sont de type LF (\n)


Normes
------
- Les commentaires des fonctions sont dans les fichiers .C. Il faut considérer les .H comme l'index d'un livre. Un fichier .H est un résumé simpliste du fichier .C associé.
- Tout les constructeurs commencent par init\_....()
- Tout les destructeurs commencent par destroy\_...()
- Tout define représentant un nombre doit être remplacé par un enum.


Utilisation
-----------
Il existe 2 manières d'utiliser le programme:

#) En ligne de commande:
	Le programme execute l'automate avec la bande puis se termine.

	``mturing automate.txt 0100101``


#) Via l'interface.
	Un menu s'affiche et diverses actions sont possible.

	``mturing``


Format des automates
--------------------

::

	# Toutes les lignes commençant par # sont des commentaires.
	# Les paramètres avant la table de transition sont interchangeables !
	# On indique ici le nombre d'état
	NbEtats = 1
	# Etat initial. Il ne peut y en avoir qu'un seum
	EtatInitial = q0
	# Etat Final. ( On peut mettre plusieurs états finaux : q42, qacc, q0 )
	EtatFinal = qacc
	# L'alphabet a utiliser
	Alphabet = 0,1,_
	# Le mot vide
	Vide = _
	# Liste des transitions
	# Format d'une ligne :
	# Pour un état donné q0, voici la liste des actions :
	# Si lecture de 0 alors :
	#                  - écrire 1
	#                  - se déplacer à droite (R) (NOTE : R : Right,  L : Left, N : Nop)
	#                  - passer dans l'état q0
	# Si lecture de 1 alors :
	#                  - écrire 0
	#                  - se déplacer à droite (R) (NOTE : R : Right,  L : Left, N : Nop)
	#                  - passer dans l'état q0
	# Si lecture de _ alors :
	#                  - écrire _
	#                  - NE pas se déplacer (N) (NOTE : R : Right,  L : Left, N : Nop)
	#                  - passer dans l'état qacc
	q0 0,1,R,q0 1,0,R,q0 _,_,N,qacc


Documentation
-------------
Le code a entièrement documenté avec doxygen.
Pour générer la documentation:

``./doxygen``