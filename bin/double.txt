################################################################################
# Format des bandes:
#	nombre -> |nombre|_|resultat|
#	Ex:
#		|1|0|  ->  |1|0|_|0|1|0|
#
# Conçut conjointement avec Léa DECK
################################################################################

#NOMBRE -- d'états de la machine de Turing
NbEtats = 13
#ALPHABET -- de la machine de Turing
Alphabet = 0,1,_
#CARACTERE -- correspondant à une cellule vide
Vide = _
#ETAT -- initial
EtatInitial = q0
#ETAT -- final
EtatFinal = qacc

#TABLE DE TRANSITIONS

q0 0,_,R,q2 1,_,R,q1 _,_,R,qacc
q1 0,0,R,q1 1,1,R,q1 _,_,R,q3
q2 0,0,R,q2 1,1,R,q2 _,_,R,q4
q3 0,0,R,q13 1,1,R,q13 _,0,R,q14
q13 0,0,R,q13 1,1,R,q13 _,1,L,q5
q14 0,0,R,q11 1,1,R,q11 _,1,L,q5
q4 0,0,R,q11 1,1,R,q11 _,0,R,q12
q11 0,0,R,q11 1,1,R,q11 _,0,L,q6
q12 0,0,R,q12 1,1,R,q12 _,0,L,q6
q5 0,0,L,q5 1,1,L,q5 _,_,L,q7
q6 0,0,L,q6 1,1,L,q6 _,_,L,q8
q7 0,0,L,q7 1,1,L,q7 _,1,R,q0
q8 0,0,L,q8 1,1,L,q8 _,0,R,q0
