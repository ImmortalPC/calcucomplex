################################################################################
# Format des bandes:
#	|nombre1|_|_|nombre2| -> |nombre1|_|nombre2|_|_|resultat|
#	Ex:
#		|0|1|0|_|_|1|0|1| -> |0|1|0|_|1|0|1|_|_|1|1|1|
#
# Conçut conjointement avec Léa DECK
################################################################################


#NOMBRE -- d'états de la machine de Turing
NbEtats = 33
#ALPHABET -- de la machine de Turing
Alphabet = 0,1,_
#CARACTERE -- correspondant à une cellule vide
Vide = _
#ETAT -- initial
EtatInitial = q0
#ETAT -- final
EtatFinal = qacc

#TABLE DE TRANSITIONS
q0 0,_,R,q16 1,_,R,q1 _,_,R,q31
q1 0,0,R,q1 1,1,R,q1 _,_,R,q2
q2 0,0,R,q2 1,1,R,q2 _,_,R,q3
q3 0,_,L,q12 1,_,L,q4 _,_,R,q3
q4 0,0,R,q4 1,1,R,q4 _,1,R,q5
q5 0,0,R,q5 1,1,R,q5 _,_,R,q6
q6 0,0,R,q6 1,1,R,q6 _,_,R,q7
q7 0,0,R,q7 1,1,R,q7 _,0,L,q8
q8 0,0,L,q8 1,1,L,q8 _,_,L,q9
q9 0,0,L,q9 1,1,L,q9 _,_,L,q10
q10 0,0,L,q10 1,1,L,q10 _,_,L,q11
q11 0,0,L,q11 1,1,L,q11 _,1,R,q0
q12 0,0,R,q12 1,1,R,q12 _,0,R,q13
q13 0,0,R,q13 1,1,R,q13 _,_,R,q14
q14 0,0,R,q14 1,1,R,q14 _,_,R,q15
q15 0,0,R,q15 1,1,R,q15 _,1,L,q8
q16 0,0,R,q16 1,1,R,q16 _,_,R,q17
q17 0,0,R,q17 1,1,R,q17 _,_,R,q18
q18 0,_,L,q19 1,_,L,q23 _,_,R,q18
q19 0,0,R,q19 1,1,R,q19 _,0,R,q20
q20 0,0,R,q20 1,1,R,q20 _,_,R,q21
q21 0,0,R,q21 1,1,R,q21 _,_,R,q22
q22 0,0,R,q22 1,1,R,q22 _,0,L,q27
q23 0,0,R,q23 1,1,R,q23 _,1,R,q24
q24 0,0,R,q24 1,1,R,q24 _,_,R,q25
q25 0,0,R,q25 1,1,R,q25 _,_,R,q26
q26 0,0,R,q26 1,1,R,q26 _,1,L,q27
q27 0,0,L,q27 1,1,L,q27 _,_,L,q28
q28 0,0,L,q28 1,1,L,q28 _,_,L,q29
q29 0,0,L,q29 1,1,L,q29 _,_,L,q30
q30 0,0,L,q30 1,1,L,q30 _,0,R,q0
q31 0,0,R,q31 1,1,R,q31 _,_,R,q32
q32 0,0,R,q32 1,1,R,q32 _,_,R,qacc
