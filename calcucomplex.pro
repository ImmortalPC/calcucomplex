######################################################################
# Automatically generated by qmake (2.01a) Sat Nov 10 19:08:33 2012
######################################################################

# Suppresion des entetes de Qt
QT -= qt gui core
CONFIG -= qt

# Ajout de flags
QMAKE_CFLAGS += -Wextra -Wunused -Wuninitialized -Wunused-variable -Wundef -Wmissing-declarations -Wmissing-prototypes -Wredundant-decls -Wshadow -Wbad-function-cast -Wcast-qual

TEMPLATE = app
DEPENDPATH += . src
INCLUDEPATH += .

# Changement des dossiers de destination
DESTDIR = bin/
MOC_DIR = obj/
RCC_DIR = obj/
UI_DIR = obj/
release:OBJECTS_DIR = obj/release/
debug:OBJECTS_DIR = obj/debug/

# Activation des mode debug & release
CONFIG += debug_and_release

CONFIG(debug, debug|release) {
		TARGET = mturing_debug
}else {
		TARGET = mturing
		# Supression total des symboles
		QMAKE_LFLAGS += -s
}

# Input
SOURCES += src/main.c \
	src/Band.c \
	src/Cell.c \
    src/Machine.c \
    src/Transition.c \
    src/AutoFile.c \
    src/StateList.c \
    src/String.c

HEADERS += \
	src/Band.h \
	src/Cell.h \
    src/config.h \
    src/Machine.h \
    src/Transition.h \
    src/State.h \
    src/AutoFile.h \
    src/StateList.h \
    src/String.h
