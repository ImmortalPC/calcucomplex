#include "AutoFile.h"

/***************************************************************************//*!
* @brief Permet de sauter les espaces et les commentaires
* @param[in,out] fp		Le pointeur de fichier
* @return[NONE]
*/
void removeCommentaryAndSpaces( FILE* fp )
{
	char c = fgetc(fp);

	while( !(('A' <= c && c <= 'Z') || ('a' <= c && c <= 'z')) )
	{
		if( c == '#' || c == ' ' || c == '\t' )
			while( !feof(fp) && fgetc(fp) != '\n' );

		c = fgetc(fp);
	}

	fseek(fp, -sizeof(char), SEEK_CUR);
}
