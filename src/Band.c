#include "Band.h"
#include "config.h"
#include <stdlib.h>
#include <string.h>


/***************************************************************************//*!
* @brief Constructeur
* @param[in] word		Le mot a utiliser pour la bande
* @param[in] emptyChar	Le mot vide
* @return La nouvelle bande
* @memberof Band
*/
Band* init_band( const char word[], char emptyChar )
{
	if( !word || !word[0] )
		stdErrorE("init(const char[]) :: word ne peut être vide !");

	Band* b = (Band*)malloc(sizeof(Band));
	b->first = 0;
	b->current = 0;
	b->emptyChar = emptyChar;

	Cell* prev = 0;
	Cell* c = 0;
	for( int i=0; word[i]; i++ )
	{
		c = init_cell(word[i]);
		if( prev )
			append_cell(prev, c);

		// On attrib le fist si pas fait
		if( !b->first )
			b->first = c;

		prev = c;
	}

	/*
	// Création d'une liste circulaire
	b->first->prev = c;
	c->next = b->first;
	//*/

	/*
	// Création d'une liste non circulaire
	b->first->prev = 0;
	c->next = 0;
	//*/

	b->current = b->first;

	return b;
}


/***************************************************************************//*!
* @brief Destructeur
* @param[in,out] b		La bande a supprimer
* @return[NONE]
* @memberof Band
*/
void destroy_band( Band* b )
{
	if( !b )
		return ;

	// Suppression des cellules
	destroy_cell(b->first);

	b->current = 0;
	b->first = 0;
	free(b);
}


/***************************************************************************//*!
* @brief Affichage de la bande
* @param[in] b		La bande a afficher
* @return[NONE]
* @memberof Band
*/
void print_band( const Band* b )
{
	Cell* c = b->first;

	if( !c ){
		stdError("print(const Band*) :: Bande vide !");
		return ;
	}

	// Affichage des data
	printf("|");
	do{
		if( c == b->current ){
			printf(" >%c< |", c->val);
		}else{
			printf(" %c |", c->val);
		}
		c = c->next;
	}while( c != 0 );
	printf("\n");
}


/***************************************************************************//*!
* @brief Déplacement de la tête de lecture vers la droite
* @param[in,out] b		La bande a modifier
* @return[NONE]
* @memberof Band
*/
void moveRight_band( Band* b )
{
	// Si on dépasse les limites de la bande => On allonge la bande
	if( !b->current->next )
		append_cell_c(b->current, b->emptyChar);
	b->current = b->current->next;
}


/***************************************************************************//*!
* @brief Déplacement de la tête de lecture vers la gauche
* @param[in,out] b		La bande a modifier
* @return[NONE]
* @memberof Band
*/
void moveLeft_band( Band* b )
{
	// Si on dépasse les limites de la bande => On allonge la bande
	if( !b->current->prev ){
		Cell* c = init_cell(b->emptyChar);
		append_cell(c,b->first);
		b->first = c;
	}
	b->current = b->current->prev;
}


/***************************************************************************//*!
* @brief Déplacement de la tête de lecture vers le début (tout à gauche)
* @param[in,out] b		La bande a modifier
* @return[NONE]
* @memberof Band
*/
void moveToFirst_band( Band* b )
{
	b->current = b->first;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la valeur courante de la bande
* @param[in] b		La bande a lire
* @return La valeur actuelle sous le cursor
* @memberof Band
*/
char currentVal_band( const Band* b )
{
	return b->current->val;
}


/***************************************************************************//*!
* @brief Permet de changer la valeur courante de la bande
* @param[in,out] b		La bande a modifier
* @param[in] val		La nouvelle valeur
* @return[NONE]
* @memberof Band
*/
void setCurrentVal_band( Band* b, char val )
{
	b->current->val = val;
}


/***************************************************************************//*!
* @brief Permet d'effectuer des tests sur Band
* @return[NONE]
* @memberof Band
* @private
*/
void test_band()
{
	Band* b = init_band("01001",'-');
	print_band(b);
	moveLeft_band(b);
	print_band(b);
	moveLeft_band(b);
	print_band(b);
	moveLeft_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	moveRight_band(b);
	print_band(b);
	destroy_band(b);
}
