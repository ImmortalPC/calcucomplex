/***************************************************************************//*!
* @file Band.h
* @brief Ce module permet de gérer une bande
* @author NUEL Guillaume
*/
#ifndef BAND_h
#define BAND_h
#include "Cell.h"


/*!
 * @brief Permet de regrouper un ensemble de Cell et de les manipuler facilement
 */
typedef struct Band {
	Cell* first;//!< @private Element de début
	Cell* current;//!< @private Element actuellement selectionné
	char emptyChar;//!< @private Le caractère VIDE. (Pour la création de nouvelle cellule)
} Band;

Band* init_band( const char word[], char emptyChar );
void destroy_band( Band* b );
void print_band( const Band* b );
void moveRight_band( Band* b );
void moveLeft_band( Band* b );
void moveToFirst_band( Band* b );
char currentVal_band( const Band* b );
void setCurrentVal_band( Band* b, char val );
void test_band();

#endif// BAND_h
