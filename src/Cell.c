#include "Cell.h"
#include <stdlib.h>


/***************************************************************************//*!
* @brief Permet de créer une cellule orpheline.
* @param[in] val	La valeur a donner a la cellule
* @return La nouvelle cellule
* @memberof Cell
* @note Pour ajouter un successeur append_cell( Cell*c, Cell* next )
*/
Cell* init_cell( char val )
{
	Cell* c = (Cell*)malloc(sizeof(Cell));
	c->val = val;
	c->next = 0;
	c->prev = 0;
	return c;
}


/***************************************************************************//*!
* @brief Destructeur
* @param[in] c	La cellule a detruire.
* @return[NONE]
* @memberof Cell
* @warning Toutes les cellules liées à {c} seront détruites
*/
void destroy_cell( Cell* c )
{
	if( !c )
		return ;
	// Suppression des cellules
	Cell* tmp=0;
	do{
		tmp = c->next;
		free(c);
		c = tmp;
	}while( c != 0 );
}


/***************************************************************************//*!
* @brief Permet d'ajouter un successeur à {c} et un prédécésseur à {next}.
* En d'autre mot, on lie 2 cellules ensemble.
* @param[in] c		La cellule parente (prev).
* @param[in] next	La cellule suivante (next).
* @return[NONE]
* @memberof Cell
*/
void append_cell( Cell*c, Cell* next )
{
	c->next = next;
	next->prev = c;
}


/***************************************************************************//*!
* @brief Permet d'ajouter un successeur à {c} et un prédécésseur à {next}.
* En d'autre mot, on lie 2 cellules ensemble.
* @param[in] c		La cellule parente (prev).
* @param[in] next	La cellule suivante (next).
* @return La cellule {next} pour permettre des chainages
* @memberof Cell
*/
Cell* append_cell_r( Cell*c, Cell* next )
{
	if( c )
		append_cell(c, next);
	return next;
}


/***************************************************************************//*!
* @brief Permet d'ajouter un successeur à {c}.
* @param[in] c		La cellule parente (prev).
* @param[in] val	La cellule suivante (next) avec la valeur {val}.
* @return La cellule qui sera crée via {val} pour permettre des chainages
* @memberof Cell
*/
Cell* append_cell_c( Cell*c, char val )
{
	Cell* next = init_cell(val);
	append_cell(c, next);
	return next;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la cellule suivante
* @param[in] c		La cellule
* @return La cellule suivante
* @memberof Cell
*/
Cell* next_cell( Cell* c )
{
	return c->next;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la cellule précédante
* @param[in] c		La cellule
* @return La cellule précédante
* @memberof Cell
*/
Cell* prev_cell( Cell* c )
{
	return c->prev;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la valeur de la cellule
* @param[in] c		La cellule
* @return La valeur de la cellule
* @memberof Cell
*/
char val_cell( const Cell* c )
{
	return c->val;
}
