/***************************************************************************//*!
* @file Cell.h
* @brief Ce module permet de gérer une cellule
* @author NUEL Guillaume
*
* Utilisation:
* @code
* Cell* c1 = init_cell('1');
* Cell* c2 = init_cell('0');
* append_cell(c1, c2);
* destroy_cell(c1);
*
* // OU
*
* c1 = init_cell('0');
* c2 = append_cell_c(c1, '1');
* c2 = append_cell_c(c2, '0');
* c2 = append_cell_c(c2, '_');
* @endcode
*/
#ifndef CELL_h
#define CELL_h


/*!
 * @brief Une cellule contient une données ainsi que son sucesseur et son prédécésseur
 * @note Les membres de Cell sont tous PUBLIC
 */
typedef struct Cell {
	char val;//!< Valeur de la cellule
	struct Cell* next;//!< Sucesseur ( vers la droite )
	struct Cell* prev;//!< Prédécésseur ( vers la gauche )
} Cell;


Cell* init_cell( char val );
void destroy_cell( Cell* c );
void append_cell( Cell*c, Cell* next );
Cell* append_cell_r( Cell*c, Cell* next );
Cell* append_cell_c( Cell*c, char val );
Cell* next_cell( Cell* c );
Cell* prev_cell( Cell* c );
char val_cell( const Cell* c );
#endif// CELL_h
