#include "Machine.h"
#include "config.h"
#include "AutoFile.h"
#include <stdio.h>
#include <stdlib.h>


/***************************************************************************//*!
* @brief Permet d'initialiser une Machine de turing
* @param[in] file		Le nom du fichier a utiliser pour l'initialisation
* @return Un pointeur vers une nouvelle machine
* @memberof Machine
* @warning Le pointeur doit être déalloué via destroy( Machine* m )
* @warning Si le fichier {file} ne peut être lut => EXIT FATAL ERROR
*/
Machine* init_machine( const char file[] )
{
	Machine* m = (Machine*)malloc(sizeof(Machine));
	m->band = 0;
	m->tr = 0;
	init_sl(&m->finalState);
	init_str(&m->alphabet);

	char c=0;
	FILE* fp=0;

	if( !(fp = fopen(file, "r")) )
		stdErrorE("Le fichier <%s> ne peut être ouvert !", file);

	char nbRead = 0;
	char nbRead_cmp = 0;
	long int pos = 0;
	unsigned int state=0;

	while( nbRead != 5 )
	{
		removeCommentaryAndSpaces(fp);
		pos = ftell(fp);
		if( fscanf(fp, "NbEtats = %u\n", &m->nbState) == 1 ){
			nbRead++;
		}else{
			fseek(fp, pos, SEEK_SET);
		}

		removeCommentaryAndSpaces(fp);
		pos = ftell(fp);
		if( fscanf(fp, "EtatInitial = q%c", &c) == 1 ){
			nbRead++;
			if( c == 'a' ){
				m->beginState = 0;
				fseek(fp, sizeof(char)*3, SEEK_CUR);// "cc\n"
			}else{
				fseek(fp, -sizeof(char), SEEK_CUR);
				if( fscanf(fp, "%u\n", &m->beginState) != 1 )
					stdErrorE("Fichier mal formé !");
				m->beginState++;
			}
			m->currentState = m->beginState;
		}else{
			fseek(fp, pos, SEEK_SET);
		}

		removeCommentaryAndSpaces(fp);
		pos = ftell(fp);
		if( fscanf(fp, "EtatFinal = q%c", &c) == 1 ){
			nbRead++;

			do{
				if( c == 'a' ){
					append_sl(&m->finalState, 0);
					fseek(fp, sizeof(char)*2, SEEK_CUR);// "cc"
				}else if( '0' <= c && c <= '9' ){
					fseek(fp, -sizeof(char), SEEK_CUR);
					if( fscanf(fp, "%u", &state) != 1 )
						stdErrorE("Fichier mal formé !");
					append_sl(&m->finalState, state+1);
				}
				c = fgetc(fp);
			}while( c != '\n' );
		}else{
			fseek(fp, pos, SEEK_SET);
		}

		removeCommentaryAndSpaces(fp);
		pos = ftell(fp);
		if( fscanf(fp, "Vide = %c\n", &m->emptyChar) == 1 ){
			nbRead++;
		}else{
			fseek(fp, pos, SEEK_SET);
		}

		removeCommentaryAndSpaces(fp);
		pos = ftell(fp);
		if( fscanf(fp, "Alphabet = %c", &c) == 1 ){
			nbRead++;
			append_str(&m->alphabet, c);
			do{
				c = fgetc(fp);
				if( c != '\n' && c != ',' && c != ' ' )
					append_str(&m->alphabet, c);
			}while( c != '\n' );
		}else{
			fseek(fp, pos, SEEK_SET);
		}

		if( nbRead == nbRead_cmp && nbRead != 5 )
			stdErrorE("Fichier mal formé !");
	}

	if( !isIn_str(&m->alphabet, m->emptyChar) )
		stdErrorE("Le caractere vide ne fait pas partie de l'alphabet !");

	m->tr = init_trs(m->nbState*size_str(&m->alphabet), fp);

	fclose(fp);

	return m;
}


/***************************************************************************//*!
* @brief Permet désinitialiser une Machine de turing
* @param[in] m		La machine de turing a supprimer
* @return[NONE]
* @memberof Machine
*/
void destroy_machine( Machine* m )
{
	if( !m )
		return ;

	if( m->band )
		destroy_band(m->band);

	if( m->tr )
		destroy_trs(m->tr);

	destroy_sl(&m->finalState);

	destroy_str(&m->alphabet);

	free(m);
}


/***************************************************************************//*!
* @brief Permet d'afficher une Machine de turing
* @param[in] m		La machine de turing a afficher
* @return[NONE]
* @memberof Machine
*/
void print_machine( const Machine* m )
{
	if( m->beginState == 0 ){
		printf("EtatInitial=qacc\n");
	}else{
		printf("EtatInitial=q%u\n", m->beginState-1);
	}

	printf("EtatFinal=");
	print_sl(&m->finalState);

	if( m->currentState == 0 ){
		printf("EtatCourant=qacc\n");
	}else{
		printf("EtatCourant=q%u\n", m->currentState-1);
	}

	printf("NbEtat=%u\n", m->nbState+1);
	print_trs(m->tr);
	if( m->band )
		print_band(m->band);
}


/***************************************************************************//*!
* @brief Permet d'afficher la bande associée à la Machine de turing
* @param[in] m		La machine de turing a afficher
* @return[NONE]
* @memberof Machine
*/
void print_machineBand( const Machine* m )
{
	if( m->band )
		print_band(m->band);
}


/***************************************************************************//*!
* @brief Permet d'executer la Machine de turing sur une bande
* @param[in] m		La machine de turing a afficher
* @return La bande obtenue. (NE PAS DESALLOUER cette bande !)
* @warning S'il n'y a pas eu de bande précédante => exit FATAL ERROR
* @memberof Machine
*/
Band* exec_lastBand( Machine* m )
{
	return exec_machine(m, 0);
}


/***************************************************************************//*!
* @brief Permet d'executer la Machine de turing sur une bande
* @param[in] m		La machine de turing a afficher
* @param[in] word	Le mot a tester. ( NOTE si word=NULL => On utilise la bande précédante)
* @return La bande obtenue. (NE PAS DESALLOUER cette bande !)
* @warning Si word est à NULL et qu'il n'y a pas eu de bande précédante => exit FATAL ERROR
* @memberof Machine
*/
Band* exec_machine( Machine* m, const char word[] )
{
	if( word ){
		if( m->band )
			destroy_band(m->band);
		m->band = init_band(word, m->emptyChar);
	}

	if( !m->band )
		stdErrorE("exec_machine():: Veuillez entrer une bande !");

	// On ré/initialise la machine et la bande
	moveToFirst_band(m->band);
	m->currentState = m->beginState;

	State_t lastState = -1;
	unsigned int nbLoop = 0;

	const Reaction* rc = 0;
	while( !isIn_sl(&m->finalState, m->currentState) )
	{
		if( !isIn_str(&m->alphabet, currentVal_band(m->band)) )
			stdErrorE("Bande invalide !\nLettre(%c) incorect vis a vis de l'alphabet attendut.", currentVal_band(m->band));

		rc = get_trs(m->tr, m->currentState, currentVal_band(m->band));
		if( rc == 0 ){
			if( m->currentState == 0 )
				stdErrorE("L'automate ne peut résoudre la bande -> Données: qacc, %c", currentVal_band(m->band));
			stdErrorE("L'automate ne peut résoudre la bande -> Données: q%u, %c", m->currentState-1, currentVal_band(m->band));

		}

		if( !isIn_str(&m->alphabet, rc->write) )
			stdErrorE("Automate non valide !\nLettre(%c) incorect vis a vis de l'alphabet attendut.", rc->write);

		m->currentState = rc->state_to;
		setCurrentVal_band(m->band, rc->write);
		switch(rc->move)
		{
			case MOVE_Right:
				moveRight_band(m->band);
				break;
			case MOVE_Left:
				moveLeft_band(m->band);
				break;
			case MOVE_Nop:
				break;
			default:
				stdErrorE("Cas improbable ! %d", rc->move);
		}

		if( lastState == m->currentState ){
			nbLoop++;
			if( nbLoop >= MAX_LOOP )
				stdErrorE("Boucle infinie !");
		}else{
			nbLoop = 0;
			lastState = m->currentState;
		}
	}

	return m->band;
}
