/***************************************************************************//*!
* @file Machine.h
* @brief Ce module permet de manipuler une machine de turing
* @author NUEL Guillaume
*
* Utilisation:
* @code
* Machine* m = init_machine("monAutomate.txt");
* print(m);
*
* Band b = exec_machine(m, bInit("01001"));
* print(b);
*
* destroy(m);
* @endcode
*/
#ifndef MACHINE_h
#define MACHINE_h
#include "Band.h"
#include "Transition.h"
#include "State.h"
#include "StateList.h"
#include "String.h"


/***************************************************************************//*!
* @brief Créer & manipuler une machine de turing
*/
typedef struct Machine {
	Band* band;//!< La bande a lire
	State_t currentState;//!< L'etat courant
	State_t beginState;//!< L'etat de depart
	StateList finalState;//!< Les etats finaux
	char emptyChar;//!< Le < caractère > vide
	String alphabet;//!< Alphabet pour la machine de turing
	unsigned int nbState;//!< Nombre d'état
	Transitions* tr;//!< Liste des transitions
} Machine;

enum {
	MAX_LOOP = 100//!< Nombre MAX de saut de qx vers qx. Permet d'arrêter les boucles infinies
};

Machine* init_machine( const char file[] );
void destroy_machine( Machine* m );
Band* exec_lastBand( Machine* m );
Band* exec_machine( Machine* m, const char word[] );
void print_machine( const Machine* m );
void print_machineBand( const Machine* m );

#endif// MACHINE_h
