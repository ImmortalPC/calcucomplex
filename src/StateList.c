#include "StateList.h"
#include <stdio.h>
#include <stdlib.h>


/***************************************************************************//*!
* @brief Constructeur
* @param[in,out] sl			La StateList
* @return[NONE]
*/
void init_sl( StateList* sl )
{
	sl->size = 0;
	sl->listState = 0;
}


/***************************************************************************//*!
* @brief Destructeur
* @param[in,out] sl			La StateList
* @return[NONE]
*/
void destroy_sl( StateList* sl )
{
	if( sl->size && sl->listState )
		free(sl->listState);
	sl->size = 0;
	sl->listState = 0;
}


/***************************************************************************//*!
* @brief Permet de d'obtenir la taille de la liste
* @param[in] sl			La StateList
* @return Taille de la liste
*/
unsigned int size_sl( const StateList* sl )
{
	return sl->size;
}


/***************************************************************************//*!
* @brief Permet de savoir si un etat est dans la liste
* @param[in] sl			La StateList
* @param[in] state		L'etat a tester
* @return 1 si state est dans la liste. 0 sinon
*/
char isIn_sl( const StateList* sl, State_t state )
{
	if( sl->size == 0 || sl->listState == 0 )
		return 0;

	for( unsigned int i=0; i<sl->size; i++ )
	{
		if( sl->listState[i] == state )
			return 1;
	}

	return 0;
}


/***************************************************************************//*!
* @brief Permet d'ajouter un element à la liste
* @param[in,out] sl			La StateList
* @param[in] state		L'etat a ajouter
* @return[NONE]
*/
void append_sl( StateList* sl, State_t state )
{
	if( sl->size == 0 || sl->listState == 0 ){
		sl->size = 1;
		sl->listState = (State_t*)malloc(sizeof(State_t)*1);
		sl->listState[0] = state;
		return ;
	}

	// On ne met pas 2 fois le même etat dans la liste
	if( isIn_sl(sl, state))
		return ;

	// Copie du tab
	State_t* lst = (State_t*)malloc(sizeof(State_t)*(sl->size+1));
	for( unsigned int i=0; i<sl->size; i++ )
		lst[i] = sl->listState[i];

	lst[sl->size] = state;
	sl->size++;

	free(sl->listState);
	sl->listState = lst;
}


/***************************************************************************//*!
* @brief Permet d'afficher la liste
* @param[in] sl			La StateList
* @return[NONE]
*/
void print_sl( const StateList* sl )
{
	for( unsigned int i=0; i<sl->size; i++ )
	{
		if( sl->listState[i] == 0 ){
			printf("qacc");
		}else{
			printf("q%u", sl->listState[i]-1);
		}
		if( i+1<sl->size )
			printf(", ");
	}
	printf("\n");
}
