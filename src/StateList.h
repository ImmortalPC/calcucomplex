#ifndef STATELIST_h
#define STATELIST_h
#include "State.h"

/***************************************************************************//*!
* @brief Permet de gérer une liste d'état.
* Cette liste est de type set. Chaque element est unique.
*/
typedef struct {
	unsigned int size;//!< Taille de la liste
	State_t* listState;//!< Liste d'etat
} StateList;

void init_sl( StateList* sl );
void destroy_sl( StateList* sl );
unsigned int size_sl( const StateList* sl );
char isIn_sl( const StateList* sl, State_t state );
void append_sl( StateList* sl, State_t state );
void print_sl( const StateList* sl );

#endif// STATELIST_h
