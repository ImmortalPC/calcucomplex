#include "String.h"
#include <stdio.h>
#include <stdlib.h>


/***************************************************************************//*!
* @brief Constructeur
* @param[in,out] str	La chaine a init
* @return[NONE]
* @memberof String
*/
void init_str( String* str )
{
	str->maxSize = 0;
	str->size = 0;
	str->word = 0;
}


/***************************************************************************//*!
* @brief Constructeur
* @param[in,out] str	La chaine a init
* @param[in] word		Utiliser le mot word pour init str
* @return[NONE]
* @memberof String
*/
void init_strW( String* str, const char word[] )
{
	// Calcule de la taille
	int size = 0;
	for( size=0; word[size]; size++ );

	str->maxSize = size+1;
	str->size = size;
	str->word = (char*)malloc(sizeof(char)*str->maxSize);
	for( unsigned int i=0; word[i]; i++ )
		str->word[i] = word[i];
}


/***************************************************************************//*!
* @brief Destructeur
* @param[in,out] str	La chaine a init
* @return[NONE]
* @memberof String
*/
void destroy_str( String* str )
{
	if( str->maxSize && str->word )
		free(str->word);

	str->maxSize = 0;
	str->size = 0;
	str->word = 0;
}


/***************************************************************************//*!
* @brief Permet de savoir si un caratère {c} est dans la chaine.
* @param[in] str	La chaine.
* @param[in] c		Le caractère
* @return 1 si {c} est trouvé dans {str}. 0 sinon
* @memberof String
*/
char isIn_str( const String* str, char c )
{
	for( unsigned int i=0; i<str->size; i++ )
	{
		if( str->word[i] == c )
			return 1;
	}
	return 0;
}


/***************************************************************************//*!
* @brief Permet de savoir si 2 chaines sont identiques
* @param[in] str1	La chaine.
* @param[in] str2	La chaine.
* @return 1 si {str1} == {str2}. 0 sinon
* @memberof String
*/
char isEqual_str( const String* str1, const String* str2 )
{
	if( str1->size != str2->size )
		return 0;

	for( unsigned int i=0; i<str1->size; i++ )
		if( str1->word[i] != str2->word[i] )
			return 0;

	return 1;
}


/***************************************************************************//*!
* @brief Permet d'ajouter {c} à la fin de {str}
* @param[in] str	La chaine.
* @param[in] c		Le caractère a ajouter
* @return[NONE]
* @memberof String
*/
void append_str( String* str, char c )
{
	if( str->maxSize ){
		if( str->maxSize > str->size+1 ){
			str->word[str->size] = c;
			str->size++;
		}else{
			str->maxSize += 5;
			char* n = (char*)malloc(sizeof(char)*str->maxSize);

			for( unsigned int i=0; i<str->size; i++ )
				n[i] = str->word[i];

			n[str->size] = c;

			for( unsigned int i=str->size+1; i<str->maxSize; i++ )
				n[i] = 0;

			free(str->word);
			str->word = n;
			str->size++;
		}

		return ;
	}


	str->maxSize = 2;
	str->size = 1;
	str->word = (char*)malloc(sizeof(char)*str->maxSize);
	for( unsigned int i=0; i<str->maxSize; i++ )
		str->word[i] = 0;
	str->word[0] = c;
}


/***************************************************************************//*!
* @brief Permet d'obtenir la taille de la chaine
* @param[in] str	La chaine.
* @return La taille de la chaine de caractère
* @memberof String
*/
unsigned int size_str( const String* str )
{
	return str->size;
}


/***************************************************************************//*!
* @brief Permet de tester le module String
* @return[NONE]
* @memberof String
* @private
*/
void test_str()
{
	String s;
	init_strW(&s, "sal");
	printf("%s\n", s.word);
	append_str(&s, 'u');
	append_str(&s, 't');
	append_str(&s, '!');
	append_str(&s, '!');
	append_str(&s, '!');
	append_str(&s, '\n');
	append_str(&s, 'c');
	append_str(&s, 'a');
	append_str(&s, ' ');
	append_str(&s, 'v');
	append_str(&s, 'a');
	append_str(&s, '?');
	printf("%s\n", s.word);
	destroy_str(&s);
}
