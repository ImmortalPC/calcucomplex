#ifndef STRING_h
#define STRING_h

/***************************************************************************//*!
* @brief Permet de gérer une chaine de caractère dynamique
*/
typedef struct {
	unsigned int size;//!< @private Taille actuel de la chaine de caractère
	unsigned int maxSize;//!< @private Taille max utilisable avant de réallouer
	char* word;//!< @private Chaine de caractère
} String;

void init_str( String* str );
void init_strW( String* str, const char word[] );
void destroy_str( String* str );
char isIn_str( const String* str, char c );
char isEqual_str( const String* str1, const String* str2 );
void append_str( String* str, char c );
void test_str();
unsigned int size_str( const String* str );


#endif// STRING_h
