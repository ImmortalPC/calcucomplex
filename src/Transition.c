#include "Transition.h"
#include "config.h"

/***************************************************************************//*!
* @brief Permet d'initialiser une liste de Transition
* @param[in] nbTr		Le nombre de transition
* @param[in,out] fp		Le fichier de configuration a lire. (fclose ne sera pas appelé)
* @return[NONE]
* @memberof Transitions
*/
Transitions* init_trs( unsigned int nbTr, FILE* fp )
{
	Transitions* tr = (Transitions*)malloc(sizeof(Transitions));

	tr->nbTr = nbTr;

	if( nbTr == 0 ){
		tr->trs = 0;
		return tr;
	}
	tr->trs = (Transition*)malloc(sizeof(Transition)*nbTr);

	// Lecture depuis le fichier
	unsigned int state, finalState;
	char read, write, move;
	char c;
	unsigned int i = 0;
	unsigned int line = 1;
	int nbRead = 0;

	while( !feof(fp) )
	{
		c = getc(fp);
		if( c == 'q' ){
			c = fgetc(fp);
			// Final State !
			if( c == 'a' ){
				state = 0;
				fseek(fp, sizeof(char)*3, SEEK_CUR);
			}else{
				fseek(fp, -sizeof(char), SEEK_CUR);
				if( fscanf(fp, "%u ", &state) != 1 )
					stdErrorE("Fichier mal formé (line %u) !", line);
				state++;
			}

			do{
				if( i >= nbTr )
					stdErrorE("Nombre de transitions incorrect! Avez vous bien spécifié le nombre d'état ?");

				if( (nbRead=fscanf(fp, "%c,%c,%c,q", &read, &write, &move)) != 3 ){
					char c1=0, c2=0, c3=0, c4=0, c5=0;
					c1=fgetc(fp);
					c2=fgetc(fp);
					c3=fgetc(fp);
					c4=fgetc(fp);
					c5=fgetc(fp);
					stdErrorE("Erreur de lecture !\nErreur au niveau de <%c%c%c%c%c> ligne %u, read=%d\n",c1,c2,c3,c4,c5, line, nbRead);
				}

				c = fgetc(fp);
				// Final State !
				if( c == 'a' ){
					finalState = 0;
					fseek(fp, sizeof(char)*2, SEEK_CUR);// 'cc'
				}else{
					fseek(fp, -sizeof(char), SEEK_CUR);
					if( fscanf(fp, "%u", &finalState) != 1 )
						stdErrorE("Fichier mal formé (line %u) !", line);
					finalState++;
				}

				tr->trs[i].state_from = (State_t)state;
				tr->trs[i].read = read;
				tr->trs[i].rc.write = write;
				tr->trs[i].rc.state_to = finalState;
				switch(move){
					case 'r':
					case 'R':
						tr->trs[i].rc.move = MOVE_Right;
						break;
					case 'l':
					case 'L':
						tr->trs[i].rc.move = MOVE_Left;
						break;
					case 'N':
					case 'n':
						tr->trs[i].rc.move = MOVE_Nop;
						break;
				}

				c = fgetc(fp);
				i++;
			}while( !feof(fp) && c != '\n' );

		}else if( c == '#' ){
			while( !feof(fp) && fgetc(fp) != '\n' );
		}
		line++;
	}

	if( i != nbTr )
		stdErrorE("Nombre de transitions incorrect !\nFichier mal formé !");

	return tr;
}


/***************************************************************************//*!
* @brief Destructeur
* @param[in,out] tr		La liste de transition a détruire
* @return[NONE]
* @memberof Transitions
*/
void destroy_trs( Transitions* tr )
{
	if( !tr )
		return ;

	if( tr->nbTr && tr->trs )
		free(tr->trs);

	free(tr);
}


/***************************************************************************//*!
* @brief Permet de connaitre l'etat d'arrivé et les actions a effectuer
* @param[in] tr				La liste de transition
* @param[in] state_from		Etat de départ
* @param[in] read			Le mot de l'aphabet a tester
* @return SI TOUT EST OK: Réponse vis à vis des transitions. Si pas de transitions
* correspondant a la recherche => NULL
* @memberof Transitions
*/
const Reaction* get_trs( const Transitions* tr, State_t state_from, unsigned char read )
{
	for( unsigned int i=0; i<tr->nbTr; i++ )
	{
		if( tr->trs[i].state_from == state_from && tr->trs[i].read == read )
			return &(tr->trs[i].rc);
	}
	return 0;
}


/***************************************************************************//*!
* @brief Afficher la liste des transitions
* @param[in] tr		La liste de transition
* @return[NONE]
* @memberof Transitions
*/
void print_trs( const Transitions* tr )
{
	if( !tr )
		return ;

	printf("NbTransitions=%u\n", tr->nbTr);

	for( unsigned char i=0; i<tr->nbTr; i++ )
		print_tr(tr->trs+i);
}


/***************************************************************************//*!
* @brief Afficher une transition
* @param[in] tr		La transition
* @return[NONE]
* @memberof Transitions
* @private
*/
void print_tr( const Transition* tr )
{
	if( tr->rc.state_to == 0 ){// Qacc
		if( tr->state_from == 0 ){// Qacc
			printf("qacc -> ... %c,%c,%c ... -> qacc\n", tr->read, tr->rc.write,toChar(tr->rc.move));
		}else{
			printf("q%u -> ... %c,%c,%c ... -> qacc\n", tr->state_from-1, tr->read, tr->rc.write,toChar(tr->rc.move));
		}
	}else{
		if( tr->state_from == 0 ){// Qacc
			printf("qacc -> ... %c,%c,%c ... -> q%u\n", tr->read, tr->rc.write,toChar(tr->rc.move), tr->rc.state_to-1 );
		}else{
			printf("q%u -> ... %c,%c,%c ... -> q%u\n", tr->state_from-1, tr->read, tr->rc.write,toChar(tr->rc.move), tr->rc.state_to-1 );
		}
	}
}


/***************************************************************************//*!
* @brief Permet de convertir un enum (Move_t) en char
* @param[in] m		La valeur a transformer
* @return La valeur transformée
* @private
* @memberof Transitions
*/
char toChar( Move_t m )
{
	switch( m )
	{
		case MOVE_Right:
			return 'R';
		case MOVE_Left:
			return 'L';
		case MOVE_Nop:
			return 'N';
	}
	return 'N';
}
