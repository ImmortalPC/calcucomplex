/***************************************************************************//*!
* @file Transition.h
* @brief Ce module permet de manipuler une liste de Transition.
* @author NUEL Guillaume
*
* @warning NE pas utiliser directement ! Veuiller passer par Machine
*/
#ifndef TRANSITION_h
#define TRANSITION_h
#include <stdio.h>
#include "State.h"


/***************************************************************************//*!
* @brief Permet de représenter un mouvement
* @memberof Transitions
*/
typedef enum {
	MOVE_Right=0,//!< Déplacement vers la droite
	MOVE_Left=1,//!< Déplacement vers la gauche
	MOVE_Nop=2//!< Pas de déplacement
} Move_t;


/***************************************************************************//*!
* @brief Permet de représenter une réaction vis à vis d'une requete
* get( const Transitions* tr, State_t state_from, unsigned char alpha );
* @memberof Transitions
*/
typedef struct Reaction {
	State_t state_to;//!< Etat d'arrivé
	unsigned char write;//!< Donnée a écrire sur la bande
	Move_t move;//!< Déplacement a effectuer
} Reaction;


/***************************************************************************//*!
* @brief Représentation d'UNE transition
* @private
* @memberof Transitions
*/
typedef struct Transition {
	State_t state_from;//!< Etat de départ
	unsigned char read;//!< Donnée a lire sur la bande
	Reaction rc;//!< Réaction sur la bande
} Transition;


/***************************************************************************//*!
* @brief Représentation d'UNE liste de transition
*/
typedef struct Transitions {
	unsigned int nbTr;//!< @private Nombre de transition
	Transition* trs;//!< @private Tableau 1D contenant les transitions
} Transitions;


Transitions* init_trs( unsigned int nbTr, FILE* fp );
void destroy_trs( Transitions* tr );
const Reaction* get_trs( const Transitions* tr, State_t state_from, unsigned char read );
void print_trs( const Transitions* tr );
void print_tr( const Transition* tr );
char toChar( Move_t m );

#endif// TRANSITION_h
