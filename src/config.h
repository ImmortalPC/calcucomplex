#ifndef CONFIG_H
#define CONFIG_H

/*******************************************************************************
* Système de debug
*/
#include <stdio.h>
#include <stdlib.h>

// NOTE: utiliser fflush(stderr) pour afficher directe les données


/*!
 * @brief Permet d'afficher une erreur <b>SANS</b> EXIT
 * @param[in] msg	{String} Le message a afficher
 * @param[in] ...	{Std} Les arguments. (Cette fonction ce comporte comme printf)
 * @return[NONE]
 */
#define stdError( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); fflush(stderr);}

/*!
 * @brief Permet d'afficher une erreur <b>AVEC</b> EXIT
 * @param[in] msg	{String} Le message a afficher
 * @param[in] ...	{Std} Les arguments. (Cette fonction ce comporte comme printf)
 * @return[NONE]
 */
#define stdErrorE( msg, ... ) {fprintf(stderr, "[file " __FILE__ ", line %d]: " msg "\n", __LINE__, ##__VA_ARGS__); fflush(stderr); exit(__LINE__);}

/*!
 * @brief Permet de rediriger la sortie d'erreur standard vers un fichier
 * @param[in] filename	{String} Le fichier a utiliser.
 * @return[NONE]
 * @warning Le fichier indiqué sera automatiquement vidé de son contenu à chaque démarage du programme.
 */
#define redirectSdtErr( filename ) freopen(filename, "w", stderr)

/*!
 * @brief Permet de fermer la sortie standard des erreurs
 * @return[NONE]
 */
#define closeSdtErr() fclose(stderr)

#endif // CONFIG_H
