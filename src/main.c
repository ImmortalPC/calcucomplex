#include "Machine.h"
#include "String.h"

enum {
	MAX_FILE_NAME = 50,
	MAX_BAND_SIZE = 101
};


/***************************************************************************//*!
* @brief printMainMenu
* @param[in] machineName		Le nom de la machine actuel. (NULL si pas de machine)
* @param[in] errorMsg			Le message d'erreur a afficher. (NULL si pas de message)
* @param[in] m					La machine de turing a afficher.
* @param[in] showFullMachine	Si TRUE => On affiche toute la machine. SINON => on affiche que la bande
* @return[NONE]
*/
void printMainMenu( const char machineName[], const char errorMsg[], const Machine* m, char showFullMachine )
{
	if( !machineName || !machineName[0] )
		machineName = "\033[1m\033[31m/!\\Pas de Machine/!\\\033[0m";// \033[1m = Highlight, \033[31m = Red, \033[0m = disable Red && Highlight

	if( !errorMsg || !errorMsg[0] )
		errorMsg = "";

	printf(
	"\033c"// Clean Screen
	"\033[4m------------------ Machine de Turing ------------------\033[0m\n"// 4m => Underscore
	"	%s\n"
	"	Machine Actuel: <%s>\n"
	"	1) Charger une machine depuis la liste\n"
	"	2) Charger une machine depuis un fichier\n"
	"	3) Executer une bande sur la machine\n"
	"	4) Afficher la machine actuel\n"
	"	q) Quitter\n"
	,errorMsg, machineName);
	if( m ){
		if( showFullMachine ){
			print_machine(m);
		}else{
			print_machineBand(m);
		}
	}
	printf(":/> ");
	fflush(stdout);
}


/***************************************************************************//*!
* @brief Point d'entrée
*
* Utilisation:
* @code
* mturing double.txt 10
* // OU
* mturing
* @endcode
*/
int main( int argc, char* argv[] )
{
	//test_band();
	//test_str();

	// Permet de changer facilement la taille pour le nom de fichier
	char machineNameParser[15] = {0};
	sprintf(machineNameParser, "%%%ds", MAX_FILE_NAME-1);
	char curentMachine[MAX_FILE_NAME] = {0};//!< Nom de la machine en cours
	const char* staticMachineViewer = "";

	// Permet de changer facilement la taille max d'une bande
	// NOTE: Il existe une taille max pour stdin (l'entrée d'une bande).
	// Une bande en elle même est de taille <illimitée>
	char bandParser[15] = {0};
	sprintf(bandParser, "%%%ds", MAX_BAND_SIZE-1);
	char curentBand[MAX_BAND_SIZE] = {0};

	const char* errorMsg = 0;//!< Permet d'afficher des messages d'erreur
	Machine* m = 0;//!< Machine de turing en cours
	Machine* mToShow = 0;//!< Si diff de NULL => On l'affiche

	char pos=0, c=0;
	char showFullMachine=0;//!< bool

	char breakSecondLoop = 0;//!< bool


	/***************************************************************************
	* Système en ligne de commande
	*/
	if( argc == 3 ){
		m = init_machine(argv[1]);
		print_machine(m);
		exec_machine(m, argv[2]);
		print_machineBand(m);
		destroy_machine(m);
		m = 0;
		return 0;

	}else if( argc != 1 ){
		fprintf(stderr, "Usage: %s automate.txt Band\n", argv[0]);
		return 0;
	}


	do{
		printMainMenu(staticMachineViewer, errorMsg, mToShow, showFullMachine);
		errorMsg = "";
		mToShow = 0;
		showFullMachine = 0;

		switch( getc(stdin) )
		{
			/*******************************************************************
			* Quitter
			*/
			case 'q':
			case 'Q':
				destroy_machine(m);
				return 0;


			/*******************************************************************
			* Charger un automate
			*/
			case '1':
				while( getc(stdin) != '\n' );

				if( !errorMsg || !errorMsg[0] )
					errorMsg = "";

				do{
					breakSecondLoop = 0;

					printf(
						"\033c"// Clean Screen
						"\033[4m------------------ Machine de Turing ------------------\033[0m\n"// 4m => Underscore
						"\n"
						"Automate a charger:\n"
						"	%s\n"
						"	1) Replace:		Format de la bande |1|0|0|1|\n"
						"	2) Double:		Format de la bande |1|0|\n"
						"	3) Xor:			Format de la bande |1|0|_|_|1|0| (/!\\ nombre1 et nombre2 DOIVENT être de même taille)\n"
						"	4) Addition:	Format de la bande |1|0|_|1|0| (/!\\ nombre1 et nombre2 DOIVENT être de même taille)\n"
						"	5) Addition v2:	Format de la bande |1|0|_|_|1|0|\n"
						"	M) Menu\n"
						":/> ",
						errorMsg
					);
					switch( getc(stdin) )
					{
						case '1':// replace
							if( m )
								destroy_machine(m);
							staticMachineViewer = "replace.txt";
							m = init_machine(staticMachineViewer);
							errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
							breakSecondLoop = 1;
							break;
						case '2':// double
							if( m )
								destroy_machine(m);
							staticMachineViewer = "double.txt";
							m = init_machine(staticMachineViewer);
							errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
							breakSecondLoop = 1;
							break;
						case '3':// xor
							if( m )
								destroy_machine(m);
							staticMachineViewer = "xor.txt";
							m = init_machine(staticMachineViewer);
							errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
							breakSecondLoop = 1;
							break;
						case '4':// addition
							if( m )
								destroy_machine(m);
							staticMachineViewer = "addition.txt";
							m = init_machine(staticMachineViewer);
							errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
							breakSecondLoop = 1;
							break;
						case '5':// addition 2
							if( m )
								destroy_machine(m);
							staticMachineViewer = "addition_v_lea.txt";
							m = init_machine(staticMachineViewer);
							errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
							breakSecondLoop = 1;
							break;

						case 'm':
						case 'M':
						case 'q':
						case 'Q':
							errorMsg = "";
							breakSecondLoop = 1;
							break;

						default:
							errorMsg = "\033[1m\033[31mCommande incorrect.\033[0m";
							break;
					}
				}while( !breakSecondLoop );
				break;


			/*******************************************************************
			* Charger un automate
			*/
			case '2':
				if( m ){
					destroy_machine(m);
					m = 0;
				}
				while( getc(stdin) != '\n' );
				printf("Nom est adresse du fichier a charger:\n:/> ");
				if( scanf(machineNameParser, curentMachine) == 1 ){
					m = init_machine(curentMachine);
					staticMachineViewer = curentMachine;
					errorMsg = "\033[1m\033[32mMachine chargée.\033[0m";
				}else{
					errorMsg = "\033[1m\033[31m/!\\ Erreur lors du chargement de la machine !\033[0m";
				}
				break;


			/*******************************************************************
			* Executer une bande sur l'automate
			*/
			case '3':
				if( !m ){
					errorMsg = "\033[1m\033[31m/!\\ Veuiller charger une machine de turing avant !\033[0m";
					break;
				}

				pos = 0;
				if( curentBand[0] ){
					printf("Bande a executer (%d caractère max):\n[ENTRER pour réutiliser la bande précédante]:/> ", MAX_BAND_SIZE-1);
					while( getc(stdin) != '\n' );
					if( (c=getc(stdin)) == '\n' ){
						exec_lastBand(m);
						mToShow = m;
						errorMsg = "";
						continue;// On remonte direct dans au niveau du do
					}
					curentBand[0] = c;
					int i=1;
					// On lit la stdin à la main
					for( i=1; i<MAX_BAND_SIZE && (c=getc(stdin)) != '\n'; i++ )
						curentBand[i] = c;

					// On remplis le reste de la chaine avec des 0
					for( ; i<MAX_BAND_SIZE; i++ )
						curentBand[i] = 0;

					exec_machine(m,curentBand);
					mToShow = m;
					errorMsg = "";
					continue;// On remonte direct dans au niveau du do
				}else{
					printf("Bande a executer (%d caractère max):\n:/> ", MAX_BAND_SIZE-1);
				}

				if( scanf(bandParser, curentBand+pos) == 1 ){
					exec_machine(m,curentBand);
					mToShow = m;
					errorMsg = "";
				}else{
					errorMsg = "\033[1m\033[31m/!\\ Erreur lors de la lecture /!\\\033[0m";
				}
				break;


			/*******************************************************************
			* Afficher l'automate
			*/
			case '4':
				if( !m ){
					errorMsg = "\033[1m\033[31m/!\\ Veuiller charger une machine de turing avant !\033[0m";
					break;
				}
				mToShow = m;
				showFullMachine = 1;
				break;


			/*******************************************************************
			* Problème de lecture
			*/
			case EOF:
				printf("Votre terminal ne supporte pas stdin\n");
				return __LINE__;


			/*******************************************************************
			* Entrée incorrect
			*/
			default:
				errorMsg = "\033[1m\033[31mCommande incorrect.\033[0m";
				break;
		}
		while( getc(stdin) != '\n' );// On vire le \n
	}while(1);

	return 0;
}
